from django.contrib.auth.mixins import (
    LoginRequiredMixin,
    UserPassesTestMixin
)
from django.views.generic import ListView, DetailView
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from .models import Post
from django.urls import reverse_lazy


# Create your views here
# LoginRequireMixin is used for authorization without login user can't visit further
# UserPassesTestMixin is used user can't delete/update others post .


class PostListView(LoginRequiredMixin, ListView):
    model = Post
    template_name = 'post/post_list.html'
    login_url = 'login'


class PostDetailView(LoginRequiredMixin, DetailView):
    model = Post
    template_name = 'post/post_detail.html'
    login_url = 'login'


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    template_name = 'post/post_update.html'
    fields = ('title', 'body')
    login_url = 'login'

    def test_func(self):
        obj = self.get_object()
        return obj.author == self.request.user


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    template_name = 'post/post_delete.html'
    success_url = reverse_lazy('post_list')
    login_url = 'login'

    def test_func(self):
        obj = self.get_object()
        return obj.author == self.request.user


class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    template_name = 'post/post_create.html'
    fields = ('title', 'body')
    login_url = 'login'

    # this function will get the current user as author for creating post
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


